import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class WeatherData {
  final String cityName;
  final double temperature;
  final String condition;
  final String iconUrl;
  final int humidity;
  final double windSpeed;

  WeatherData({
    required this.cityName,
    required this.temperature,
    required this.condition,
    required this.iconUrl,
    required this.humidity,
    required this.windSpeed,
  });
}

class WeatherProvider with ChangeNotifier {
  WeatherData? weatherData;
  bool isLoading = false;
  String? error;

  final String apiKey = 'YOUR_API_KEY';

  // Fetch weather data for a given city
  Future<bool> fetchWeather(String cityName) async {
    _setLoading(true);
    error = null;

    try {
      final response = await http.get(
        Uri.parse(
          'https://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$apiKey&units=metric',
        ),
      );

      if (response.statusCode == 200) {
        final data = json.decode(response.body);
        weatherData = WeatherData(
          cityName: data['name'],
          temperature: data['main']['temp'],
          condition: data['weather'][0]['description'],
          iconUrl:
              'http://openweathermap.org/img/w/${data['weather'][0]['icon']}.png',
          humidity: data['main']['humidity'],
          windSpeed: data['wind']['speed'],
        );
        _saveLastCity(cityName);
        _setLoading(false);
        return true;
      } else {
        error = 'Error: ${response.reasonPhrase}';
      }
    } catch (e) {
      error = 'Error: $e';
    }

    _setLoading(false);
    return false;
  }

  // Refresh weather data for the last searched city
  Future<void> refreshWeather() async {
    if (weatherData != null) {
      await fetchWeather(weatherData!.cityName);
    }
  }

  // Set loading state and notify listeners
  void _setLoading(bool value) {
    isLoading = value;
    notifyListeners();
  }

  // Save the last searched city to SharedPreferences
  Future<void> _saveLastCity(String cityName) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('lastCity', cityName);
  }

  // Load the last searched city from SharedPreferences and fetch its weather data
  Future<void> loadLastCity() async {
    final prefs = await SharedPreferences.getInstance();
    final lastCity = prefs.getString('lastCity');
    if (lastCity != null) {
      await fetchWeather(lastCity);
    }
  }
}
