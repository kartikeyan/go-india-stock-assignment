import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'weather_provider.dart';

class WeatherScreen extends StatelessWidget {
  const WeatherScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Weather Details'),
        centerTitle: true,
        backgroundColor: Colors.blue.shade100,
        actions: [
          // Button to refresh weather data
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () {
              Provider.of<WeatherProvider>(context, listen: false)
                  .refreshWeather();
            },
          ),
        ],
      ),
      body: Consumer<WeatherProvider>(
        builder: (context, weatherProvider, child) {
          if (weatherProvider.isLoading) {
            // Show loading indicator while data is being fetched
            return const Center(child: CircularProgressIndicator());
          } else if (weatherProvider.error != null) {
            // Show error message if there was an error fetching data
            return Center(child: Text(weatherProvider.error!));
          } else if (weatherProvider.weatherData != null) {
            // Display weather information
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Card(
                    elevation: 4,
                    color: Colors.white,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(top: 15),
                              child: Text(
                                weatherProvider.weatherData!.cityName,
                                style: const TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _dataAndTitleWidget(
                              "Temperature",
                              ' ${weatherProvider.weatherData!.temperature} °C',
                            ),
                            _dataAndTitleWidget(
                              "Condition",
                              weatherProvider.weatherData!.condition,
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Image.network(
                              weatherProvider.weatherData!.iconUrl,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _dataAndTitleWidget(
                              "Humidity",
                              '${weatherProvider.weatherData!.humidity}%',
                            ),
                            _dataAndTitleWidget(
                              "Wind Speed",
                              '${weatherProvider.weatherData!.windSpeed} m/s',
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            );
          } else {
            // Show a message when there's no data
            return const Center(
              child: Text('No data'),
            );
          }
        },
      ),
    );
  }

  Widget _dataAndTitleWidget(String title, String data) {
    return Padding(
      padding: const EdgeInsets.all(18),
      child: Column(
        children: [
          Text(
            data,
            style: const TextStyle(
              fontSize: 22,
              color: Colors.black87,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            title,
            style: const TextStyle(
              color: Colors.black87,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
