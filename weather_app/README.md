# Weather App

A simple Flutter application that displays current weather information for a given city using the OpenWeatherMap API.

## Features

- Search for a city's current weather
- Display city name, current temperature, weather condition, humidity percentage, and wind speed
- Refresh weather data
- Persist the last searched city

## Screens

1. **Home Screen:**

   - Search bar to enter a city name
   - Button to trigger the weather search
   - Loading indicator while fetching data

2. **Weather Details Screen:**
   - Display city name
   - Display current temperature (in Celsius)
   - Display weather condition (e.g., cloudy, sunny, rainy)
   - Display an icon representing the weather condition
   - Display humidity percentage
   - Display wind speed
   - Refresh button to fetch updated weather data

## Requirements

- Flutter SDK
- OpenWeatherMap API Key

## Getting Started

### Prerequisites

Ensure you have Flutter installed. You can follow the installation guide [here](https://flutter.dev/docs/get-started/install).

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/kartikeyan/go-india-stock-assignment.git
   cd weather_app
   ```

2. Install the dependencies:

   ```bash
   flutter pub get
   ```

3. Replace `'YOUR_API_KEY'` with your actual OpenWeatherMap API key in `weather_provider.dart`:
   ```dart
   final String apiKey = 'YOUR_API_KEY';
   ```

### Running the App

1. Run the app on an emulator or a physical device:
   ```bash
   flutter run
   ```

### Additional Notes

- Ensure that you have a stable internet connection as the app fetches data from the OpenWeatherMap API.
- The app uses `Provider` for state management and `SharedPreferences` for persisting the last searched city.

## Project Structure

- `main.dart`: Main entry point of the application.
- `home_screen.dart`: Contains the UI and logic for the home screen.
- `weather_screen.dart`: Contains the UI and logic for the weather details screen.
- `weather_provider.dart`: Contains the logic for fetching weather data and managing the app's state.

## Dependencies

- `http`: For making HTTP requests.
- `provider`: For state management.
- `shared_preferences`: For persisting the last searched city.
